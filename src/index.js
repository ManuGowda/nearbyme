import restify from 'restify';
import bunyan from 'bunyan';
import fs from 'fs';
import {Markers} from './controllers/markers';

let log = bunyan.createLogger({name: 'nearbyme'});

let markers = new Markers(server);

let server = restify.createServer({
  name:'nearbyme',
  log: log
})

server
  .pre(restify.pre.userAgentConnection())
  .pre(restify.CORS())
  .pre(restify.fullResponse())
  .use(restify.bodyParser())
  .use(restify.queryParser())

// Markers
server.get('/markers/:category/:lat/:long', markers.listMarker);
// Markers

server.listen(3000, () => {
	server.log.info('Server listening on port 3000!!!');
})