import {Request} from '../utils/request';

const ACCESS_TOKEN = 'fcd07ab652e59dda327850f5ffaa737f';
const API_END_POINT = 'https://developers.zomato.com/api/v2.1';

let r = null;
export class Restraunt {
  constructor() {
    this.options = {
      method: 'GET',
      json: true,
      headers: {
        'user-key': ACCESS_TOKEN,
        'content-type': 'application/json'
      }
    };

    r = new Request();
  }

  search(req, res) {
    this.options.url = API_END_POINT 
                      + '/search?lat='+ parseFloat(req.params.lat) 
                      + '&lon='+ parseFloat(req.params.long);

    r.get(this.options, req, res);
  };
}