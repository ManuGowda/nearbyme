import request from 'request-promise';

export class Request {
	constructor() {
	}

	get(options, req, res) {
		req.log.debug('Request for %s with params: ', req.params.category, options);

		request.get(options).then(function(data) {  
			res.send(200, data);
		}).catch(function(err) {
			req.log.error('Error processing request for %s with params: ', req.params.category, options);
			res.send(500, {'Got an error' :  err.message});
		});
	};
}
